# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Name to specify for stock_history',
    # 'name_de_DE': '',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Description to specify for stock_history
    ''',
    # 'description_de_DE': '''
    #''',
    'depends': [
        'stock',
    ],
    'xml': [
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}

# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
import datetime
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.pool import Pool


class Move(ModelSQL, ModelView):
    _name = 'stock.move'

    pack_date = fields.DateTime('Pack Date')

    def __init__(self):
        super(Move, self).__init__()
        self.product = copy.copy(self.product)
        self.product.datetime_field = 'pack_date'
        if 'pack_date' not in self.product.depends:
            self.product.depends = copy.copy(self.product.depends)
            self.product.depends.append('pack_date')
        self._reset_columns()

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['pack_date'] = False
        return super(Move, self).copy(ids, default=default)

Move()


class ShipmentIn(ModelWorkflow, ModelSQL, ModelView):
    _name = 'stock.shipment.in'

    pack_date = fields.DateTime('Pack Date')

    def __init__(self):
        super(ShipmentIn, self).__init__()
        self.supplier = copy.copy(self.supplier)
        self.supplier.datetime_field = 'pack_date'
        if 'pack_date' not in self.supplier.depends:
            self.supplier.depends = copy.copy(self.supplier.depends)
            self.supplier.depends.append('pack_date')
        self.contact_address = copy.copy(self.contact_address)
        self.contact_address.datetime_field = 'pack_date'
        if 'pack_date' not in self.contact_address.depends:
            self.contact_address.depends = copy.copy(
                self.contact_address.depends)
            self.contact_address.depends.append('pack_date')
        self._reset_columns()

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['pack_date'] = False
        return super(ShipmentIn, self).copy(ids, default=default)

    def wkf_received(self, shipment):
        move_obj = Pool().get('stock.move')

        super(ShipmentIn, self).wkf_received(shipment)
        pack_date = datetime.datetime.now()
        self.write(shipment.id, {'pack_date': pack_date})
        move_obj.write([m.id for m in shipment.moves], {
            'pack_date': pack_date,
            })

ShipmentIn()


class ShipmentOut(ModelWorkflow, ModelSQL, ModelView):
    _name = 'stock.shipment.out'

    pack_date = fields.DateTime('Pack Date')

    def __init__(self):
        super(ShipmentOut, self).__init__()
        self.customer = copy.copy(self.customer)
        self.customer.datetime_field = 'pack_date'
        if 'pack_date' not in self.customer.depends:
            self.customer.depends = copy.copy(self.customer.depends)
            self.customer.depends.append('pack_date')
        self.delivery_address = copy.copy(self.delivery_address)
        self.delivery_address.datetime_field = 'pack_date'
        if 'pack_date' not in self.delivery_address.depends:
            self.delivery_address.depends = copy.copy(
                self.delivery_address.depends)
            self.delivery_address.depends.append('pack_date')
        self._reset_columns()

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['pack_date'] = False
        return super(ShipmentOut, self).copy(ids, default=default)

    def wkf_packed(self, shipment):
        move_obj = Pool().get('stock.move')

        super(ShipmentOut, self).wkf_packed(shipment)
        pack_date = datetime.datetime.now()
        self.write(shipment.id, {'pack_date': pack_date})
        move_obj.write([m.id for m in shipment.moves], {
            'pack_date': pack_date,
            })

ShipmentOut()


class ShipmentOutReturn(ModelWorkflow, ModelSQL, ModelView):
    _name = 'stock.shipment.out.return'

    pack_date = fields.DateTime('Pack Date')

    def __init__(self):
        super(ShipmentOutReturn, self).__init__()
        self.customer = copy.copy(self.customer)
        self.customer.datetime_field = 'pack_date'
        if 'pack_date' not in self.customer.depends:
            self.customer.depends = copy.copy(self.customer.depends)
            self.customer.depends.append('pack_date')
        self.delivery_address = copy.copy(self.delivery_address)
        self.delivery_address.datetime_field = 'pack_date'
        if 'pack_date' not in self.delivery_address.depends:
            self.delivery_address.depends = copy.copy(
                self.delivery_address.depends)
            self.delivery_address.depends.append('pack_date')
        self._reset_columns()

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['pack_date'] = False
        return super(ShipmentOutReturn, self).copy(ids, default=default)

    def wkf_received(self, shipment):
        move_obj = Pool().get('stock.move')

        super(ShipmentOutReturn, self).wkf_received(shipment)
        pack_date = datetime.datetime.now()
        self.write(shipment.id, {'pack_date': pack_date})
        move_obj.write([m.id for m in shipment.moves], {
            'pack_date': pack_date,
            })

ShipmentOutReturn()
